var playerName = '';

var Cname = document.getElementById("Chellenger-name");


if (playerName !== '') {
    Cname.innerText = playerName;
    document.getElementById("asdf").style.display = "none";
    document.getElementById("alert").style.display = "none";
} else {
    document.getElementById("asdf").placeholder = "Enter your name";
    document.getElementById("asdf").addEventListener('change', function () {
        playerName = document.getElementById("asdf").value;
        console.log(playerName);
        Cname.innerText = playerName;
        document.getElementById("asdf").style.display = "none";
        document.getElementById("alert").style.display = "none";
    });
}
var recordsRef = firebase.database().ref('records');


var mainState = {
    preload: function () {

        game.load.image('wall', 'assets/wall.png'); // 牆壁
        game.load.image('ceiling', 'assets/ceiling.png'); // 天花板的刺 
        game.load.image('normal', 'assets/normal.png'); // 藍色平台
        game.load.image('nails', 'assets/nails.png'); // 帶刺平台

        game.load.spritesheet('player', 'assets/player.png', 32, 32); // 主角
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16); // 向右捲動的平台
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16); // 向左捲動的平台
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22); // 彈簧墊
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36); // 翻轉的大平台

    },
    create: function () {

        //BGM
        bgm = game.add.audio('bgm');
        bgm.volume -= 0.8;
        bgm.loopFull();;

        //audio
        step = game.add.audio('step');
        jump = game.add.audio('jump');
        conveyor = game.add.audio('conveyor');
        hit = game.add.audio('hit');
        hit.volume -= 0.6;
        conveyor.volume -= 0.5;
        jump.volume -= 0.4;
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        //Keyboard
        this.keyboard = game.input.keyboard.addKeys({
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'down': Phaser.Keyboard.DOWN,
            'enter': Phaser.Keyboard.ENTER,
            'space': Phaser.Keyboard.SPACEBAR
        });
        this.CreatePlayer();
        this.createBounders();
        this.createTextsBoard();

        this.lastTime = -400;
        this.platforms = [];
        this.distance = 0;
        this.last_platform = 0;
        this.last_time = game.time.now;
        this.last_pos_x = 0;
        this.show_result = false;
    },
    CreatePlayer: function () {
        this.player = game.add.sprite(200, 50, 'player');
        this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);

        //player Animation
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);

        this.player.body.gravity.y = 500;

        this.player.life = 10;
        this.player.unbeatableTime = 0;
        this.player.touchOn = undefined;
    },
    createBounders: function () {
        // Wall
        this.leftWall = game.add.sprite(0, 0, 'wall');
        this.rightWall = game.add.sprite(game.width - 17, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);

        this.leftWall.body.immovable = true;
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        ceiling = game.add.image(0, 0, 'ceiling');
    },
    createTextsBoard: function () {
        var style = {
            fill: '#ff0000',
            fontSize: '20px'
        }
        this.text1 = game.add.text(10, 10, '', style);
        this.text2 = game.add.text(350, 10, '', style);
        this.text3 = game.add.text(140, 200, 'Enter ： 重新開始', style);
        this.text4 = game.add.text(140, 230, '空白鍵 ： 回主菜單', style)

        this.text3.visible = false;
        this.text4.visible = false;
    },
    updatePlayer: function () {
        if (this.keyboard.left.isDown) {
            this.player.body.velocity.x = -250;
        } else if (this.keyboard.right.isDown) {
            this.player.body.velocity.x = 250;
        } else if (this.keyboard.down.isDown) {
            this.player.body.gravity.y = 510;
        } else {
            this.player.body.velocity.x = 0;
        }
        this.setPlayerAnimate(this.player);
    },
    setPlayerAnimate: function (player) {
        var speed_x = player.body.velocity.x;
        var speed_y = player.body.velocity.y;

        if (speed_x < 0 && speed_y > 0) {
            player.animations.play('flyleft');

        }
        if (speed_x > 0 && speed_y > 0) {
            player.animations.play('flyright');

        }
        if (speed_x < 0 && speed_y == 0) {
            player.animations.play('leftwalk');
        }
        if (speed_x > 0 && speed_y == 0) {
            player.animations.play('rightwalk');
        }
        if (speed_x == 0 && speed_y != 0) {
            player.animations.play('fly');

        }
        if (speed_x == 0 && speed_y == 0) {
            player.frame = 8;
        }
    },
    update: function () {
        game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
        this.physics.arcade.collide(this.player, this.platforms, this.effect);

        this.checkTouchCeiling(this.player);
        this.checkGameOver();

        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();
        this.createPlatforms();

        if (this.player.body.velocity.y != 0) {
            this.last_platform = 0;
        }
    },
    updateTextsBoard: function () {
        this.text1.setText('life:' + this.player.life);
        this.text2.setText('B' + this.distance);
    },
    checkTouchCeiling: function (player) {
        if (player.body.y < 0) {
            if (player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if (game.time.now > player.unbeatableTime) {
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },
    checkGameOver: function () {
        if (this.player.life <= 0 || this.player.body.y > 500) {
            //stop bgm
            bgm.pause();
            this.text2.visible = false;
            point = this.distance;

            this.gameOver(point);
        }

    },
    gameOver: function (point) {

        var style2 = {
            fill: '#009900',
            fontSize: '40px'
        }
        this.platforms.forEach(function (s) {
            s.destroy()
        });
        this.platforms = [];
        if (!this.show_result) {
            this.text3.visible = true;
            this.text4.visible = true;
            this.text5 = game.add.text(180, 80, point + ' 樓', style2);
            this.show_result = true;
            var newRecordRef = recordsRef.push();
            newRecordRef.set({
                score: point,
                playerName: playerName
            });
        }
        if (this.keyboard.space.isDown) {
            game.state.start('menu');
        }
        if (this.keyboard.enter.isDown) {
            game.state.start('main');
        }

        //game.state.start('gameover'); todo
    },
    updatePlatforms: function () {
        for (var i = 0; i < this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if (platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },
    createPlatforms: function () {
        if (game.time.now > this.lastTime + 600) {
            this.lastTime = game.time.now;
            this.createOnePlatform();
            this.distance += 1;
        }
    },
    createOnePlatform: function () {

        var platform;
        var x = Math.random() * (400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;

        if (rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);

        platform.body.checkCollision.down = true;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },
    effect: function (player, platform) {

        if (platform.key == 'conveyorRight') {
            this.last_platform = 1;
            conveyor.play();
            conveyorRightEffect(player, platform);
        }
        if (platform.key == 'conveyorLeft') {
            this.last_platform = 2;
            conveyor.play();
            conveyorLeftEffect(player, platform);
        }
        if (platform.key == 'trampoline') {
            this.last_platform = 3;
            jump.play();
            trampolineEffect(player, platform);
        }
        if (platform.key == 'nails') {
            this.last_platform = 4;
            hit.play();
            nailsEffect(player, platform);
        }
        if (platform.key == 'normal') {

            if (this.last_platform != 5) {
                this.last_platform = 5;
                step.play();
            }
            basicEffect(player, platform);
        }
        if (platform.key == 'fake') {

            if (this.last_platform != 6) {
                this.last_platform = 6;
                step.play();
            }
            fakeEffect(player, platform);
        }

    }
};

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if (player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if (player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function () {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}
var bootState = {
    preload: function () {
        game.add.text(100, 100, "Loading ...", {
            font: "65px Arial",
            fill: "#ff0044"
        });
        game.load.video('title', 'open.mp4');
        game.load.audio('menu_music', 'assets/menu_background_music_AfterTheFall.ogg');
        game.load.audio('selectMusic', 'assets/select_music.ogg');
        game.load.image('menu', 'assets/menu.png');
    },
    create: function () {
        game.stage.backgroundColor = '#232323';
        this.video = game.add.video('title');
        this.sprite = this.video.addToWorld(-80, -40, 0, 0, 400 / 640, 1);
        this.video.play(false);

    },
    update: function () {
        if (!this.video.playing)
            game.state.start('menu');
    }
};
var menuState = {
    preload: function () {
        game.load.audio('bgm', 'assets/BGM.wav');
        game.load.audio('step', 'assets/138476__randomationpictures__step-tap.wav');
        game.load.audio('hit', 'assets/hit.wav');
        game.load.audio('jump', 'assets/jump.ogg');
        game.load.audio('conveyor', 'assets/172350_freemaster2_conveyor-belt.ogg');
    },
    create: function () {

        music = game.add.audio('menu_music');
        music.volume -= 0.7;
        music.onDecoded.add(function () {
            music.fadeIn(3000);
        }, this);
        selectMusic = game.add.audio('selectMusic');


        var sprite = game.add.image(0, 0, 'menu');
        //var tween = game.add.tween(sprite);
        //tween.to({ x: 0 }, 1000, 'Linear', true, 0);

        this.select = 0;
        this.one = game.add.text(100, 100, "Play", {
            font: "35px Arial",
            fill: "#ff0044"
        });
        this.records = game.add.text(100, 140, "Records", {
            font: "35px Arial",
            fill: "#ff0044"
        });
        this.report = game.add.text(100, 180, "My Report", {
            font: "35px Arial",
            fill: "#ff0044"
        });

        var release_down = true;
        var release_up = true;
        var release_enter = true;
        this.keyboard = game.input.keyboard.addKeys({
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'enter': Phaser.Keyboard.ENTER,
            'space':Phaser.Keyboard.SPACEBAR
        });
    },
    update: function () {

        // menu scrolling options
        if (this.keyboard.down.isDown) {
            if (release_down) {
                this.select = (this.select + 1) % 3;
                selectMusic.play();
            }
            release_down = false;
        } else if (this.keyboard.down.isUp) {
            release_down = true;
        }

        if (this.keyboard.up.isDown) {
            if (release_up) {
                this.select = this.select - 1;
                if (this.select < 0)
                    this.select = 2;
                selectMusic.play();
            }
            release_up = false;
        } else if (this.keyboard.up.isUp) {
            release_up = true;
        }
        // select playing mode
        if (this.keyboard.enter.isDown) {
            if(release_enter){
                if (this.select == 0) {

                    //selectMusic.destroy();
                    //game.cache.removeSound('selectMusic');
                    music.pause();
                    game.state.start('main');
                } else if (this.select == 1) {
                    this.one.visible = false;
                    this.records.visible = false;
                    this.report.visible = false;
    ;
                    var player = ['','','','',''];
                    var point= [0,0,0,0,0];
                    var i = 0;
                    //var sort = recordsRef.orderByChild('score').limitToLast(5);
                    var sortRef = recordsRef.orderByChild('score').limitToLast(5);
                    sortRef.once('value').then( function (snapshot){
                        snapshot.forEach(function(childSnapshot){
                            //console.log(childSnapshot.val().playerName);
                            player[i] = childSnapshot.val().playerName;
                            point[i] = childSnapshot.val().score;
                            console.log(childSnapshot.val().playerName);
                            console.log(player[i]);
                            var style2 = {
                                fill: '#ffffff',
                                fontSize: '20px'
                            }
                            game.add.text(20, 200-30*i,'第'+(5-i)+"名 : " + player[i] +' '+ point[i] +"層", style2);
                            i++;
                        });
                    });
                    var style3 = {
                        fill: '#990000',
                        fontSize: '25px'
                    }
                    game.add.text(40, 230,'空白鍵返回menu', style3);
                    //console.log(player[0]+','+point[0]);
                    
                } else {
    
                    window.location.href = "https://hackmd.io/s/BJiMIYfJQ#"; 
                }
                release_enter = false;
            }
        }else if(this.keyboard.enter.isUp){
            release_enter = true;
        }

        if(this.keyboard.space.isDown){
            music.pause();
            game.state.start('menu');
        }

        this.one.fill = "#ff0044";
        this.records.fill = "#ff0044";
        this.report.fill = "#ff0044";

        if (this.select == 0) {
            this.one.fill = "#009944";
        }else if (this.select == 1) {
            this.records.fill = "#009944";
        } else if (this.select == 2) {
            this.report.fill = "#009944";
        }
    }
}


var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
game.state.add('boot', bootState);
game.state.add('menu', menuState);
game.state.add('main', mainState);

game.state.start('boot');
